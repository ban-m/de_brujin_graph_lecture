extern crate rand;
use std::io::{BufWriter,Write};
use std::path::Path;
use std::fs;
use rand::{Rng,thread_rng};
use std::collections::HashSet;
const IS_REPEAT_ON:bool = false;
const BASES:[u8;4] = ['A' as u8,'G' as u8,'T' as u8,'C' as u8];
fn main() -> std::io::Result<()>{
    let args:Vec<_> = std::env::args().collect();
    let genome_size:usize = args[1].parse().expect("genome size");
    let read_length:usize = args[2].parse().expect("read length");
    let k:usize = args[3].parse().expect("kmer size");
    let coverage:usize = args[4].parse().expect("coverage");
    let fasta_output = Path::new(&args[5]);
    let genome_output = Path::new(&args[6]);
    let genome = generate_genome(genome_size,k,&genome_output);
    let fastas = sample(genome,read_length,coverage);
    let mut wtr = BufWriter::new(fs::File::create(&fasta_output)?);
    for (index,fasta) in fastas.into_iter().enumerate() {
        writeln!(wtr,">readid={} synthetic data for de Brujin Graph lecture",index)?;
        writeln!(wtr,"{}",std::string::String::from_utf8(fasta).unwrap());
    }
    Ok(())
}

fn generate_genome(genome_size:usize,k:usize,genome_output:&Path)->Vec<u8>{
    let mut kmer_so_far:HashSet<_> = HashSet::new();
    let mut rng = thread_rng();
    let mut genome:Vec<_> = (0..k).filter_map(|_|rng.choose(&BASES))
        .map(|e|*e)
        .collect();
    let mut current_kmer = genome.clone();
    kmer_so_far.insert(current_kmer.clone());
    for _ in 0..genome_size - k{
        let (kmer,next) = next_kmer(current_kmer,&kmer_so_far);
        current_kmer = kmer;
        kmer_so_far.insert(current_kmer.clone());
        genome.push(next);
    }
    let mut wtr = BufWriter::new(fs::File::create(genome_output).unwrap());
    writeln!(wtr,">synthetic_genome k={} created for de Brujin Graph lecture",k);
    writeln!(wtr,"{}",std::string::String::from_utf8(genome.clone()).unwrap());
    genome
}

fn next_kmer(mut previous:Vec<u8>,so_far:&HashSet<Vec<u8>>)->(Vec<u8>,u8){
    let mut rng = thread_rng();
    let kmer = previous.split_off(1);
    loop{
        let base =*rng.choose(&BASES).unwrap_or(&('A' as u8));
        let mut next = kmer.clone();
        next.push(base);
        if !so_far.contains(&next){
            return (next,base)
        }
    }
}

fn sample(genome:Vec<u8>,read_length:usize,coverage:usize)->Vec<Vec<u8>>{
    let times = coverage * genome.len() / read_length;
    let mut rng = thread_rng();
    (0..times).map(|_|rng.gen_range(0,genome.len()))
        .map(|start|if start + read_length < genome.len(){
            genome[start..start+read_length]
                .iter().map(|&e|e).collect()
        }else{
            genome[start..]
                .iter().chain(genome[..start + read_length - genome.len()].iter())
                .map(|&e|e)
                .collect()
        })
        .collect()
}
