# Generate random Genome

Author: Bansho Masutani: banmasutani@gmail.com
Date: 2018/09/25

## Synopsis
```bash
cargo run --release [genome size] [read length] [assumed k size] [coverage] [output fasta file] [output reference genome file]
```

will create [output fasta file] which is error-free [lead length] reads sampled from the genome in [ourput refenrece genome file].

Note that the genome would be VERY suited for de Brujin Graph with parameter of k=[assumed k size] because all k-mer in this genome will be certainly destinctive unless the flag of `repeat` is true.