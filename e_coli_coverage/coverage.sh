#$ -S /bin/bash
#$ -N Coverage
#$ -cwd
#$ -pe smp 12
#$ -e ./result/log-pdf
#$ -o ./result/out
#$ -V
## 2018-09-10
## Bansho Masutani

SAM=~/work/de_brujin_graph_lecture/e_coli_coverage/result/bowtie2-mapped.sam
FROM=1000000
TO=16000000
BY=4000000
#cargo run --release --bin sample_from_sam -- ${SAM} ${FROM} ${TO} ${BY}
Rscript --vanilla ./src/coverage_plot.R ${FROM} ${TO} ${BY}
