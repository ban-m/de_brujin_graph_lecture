# Ecoli coverage check

## Description
Date: 2018-09-10
Author: Bansho Masutani<banmasutani@gmail.com>

## Aim

- To see the coverage(in two means) of the dataset.
- To see the frequency of k-mer in the dataset.

## Method

- Coverage(depth/covering)
- For each subsampled dataset do in Rust language
  - calcurate coverage
  - calcurate the covered fraction
- Then, plot the coverage histogram and normalzied one.
- Do the same thing by using ordinary aligner(bowtie2?)
- For 1,2,4,8,16,32,64-mer do
  - Calculate the fraction of frequency of k-mers
  - plot


## Lang

- R
- Rust