#$ -S /bin/bash
#$ -N Coverage
#$ -cwd
#$ -pe smp 12
#$ -e ./result/log-pdf
#$ -o ./result/out
#$ -V
## 2018-09-10
## Bansho Masutani

FASTQ=/data/ban-m/ecoli/ERR022075.fastq
for k in 20 30 40 50
do
    cargo run --release --bin kmer_coverage -- ${FASTQ} ${k} > ./result/${k}mer_coverage.dat
    Rscript --vanilla ./src/kmer_coverage_plot.R ./result/${k}mer_coverage.dat ./pdf/${k}mer_coverage.pdf 
done
