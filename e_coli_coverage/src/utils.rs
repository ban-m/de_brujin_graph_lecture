use std::slice;
use std::fs::File;
use std::io;
use std::io::{BufRead,BufReader};
use regex::Regex;
use std::path::Path;
use rayon::prelude::*;
use std::cmp::max;
pub fn into_coverage(reads:&[Sam])->Vec<Coverage>{
    reads.par_iter().fold(||vec![],|acc,sam|combine_sam(acc,sam))
        .reduce(||vec![],|a,b|merge_two_coverages(a,b))
}


#[derive(Debug,Clone)]
pub struct Coverage{
    r_name:String,
    cov:Vec<(usize,u64)>
}

fn merge_two_coverages(a:Vec<Coverage>,b:Vec<Coverage>)->Vec<Coverage>{
    let mut res = Vec::with_capacity(a.len().max(b.len()));
    let mut aiter = a.into_iter().peekable();
    let mut biter = b.into_iter().peekable();
    while let (Some(arname),Some(brname)) = (aiter.peek().map(|e|e.r_name.clone())
                                             ,biter.peek().map(|e|e.r_name.clone())){
        if arname < brname {
            res.push(aiter.next().unwrap().clone());
        }else if arname > brname{
            res.push(biter.next().unwrap().clone());
        }else{
            let acov = aiter.next().unwrap();
            let bcov = biter.next().unwrap();
            res.push(acov.merge(&bcov).clone());
        }
    }
    res.extend(aiter);
    res.extend(biter);
    res
}


impl Coverage{
    pub fn r_name(&self)->&str{
        &self.r_name
    }
    pub fn get_cov(&self)->slice::Iter<(usize,u64)>{
        self.cov.iter()
    }

    fn merge(&self,cov:&Self)->Self{
        let mut res = Vec::with_capacity(max(self.cov.len(),cov.cov.len()));
        if self.r_name != cov.r_name{
            panic!("merging {:?},{:?}",self,cov);
        }else{
            let mut selfiter = self.cov.iter().peekable();
            let mut coviter = cov.cov.iter().peekable();
            while let (Some((s_index,_)),
                       Some((c_index,_))) = (selfiter.peek(),
                                                   coviter.peek()){
                if s_index < c_index {
                    res.push(selfiter.next().unwrap().clone())
                }else if s_index > c_index {
                    res.push(coviter.next().unwrap().clone())
                }else{
                    let (s_index,s_depth) = selfiter.next().unwrap();
                    let (_c_index,c_depth) = coviter.next().unwrap();
                    res.push((*s_index,s_depth+c_depth))
                }
            }
            res.extend(selfiter);
            res.extend(coviter);
        }
        Coverage{r_name:self.r_name.clone(),
                 cov:res}
    }
}

fn combine_sam(mut acc:Vec<Coverage>,sam:&Sam)->Vec<Coverage>{
    match acc.binary_search_by(|probe|probe.r_name.cmp(&sam.r_name)){
        Err(index) => acc.insert(index,sam.to_coverage()),
        Ok(index) => acc[index] = acc[index].merge(&sam.to_coverage()),
    }
    acc
}

pub fn load_sam_file(file:&Path)->io::Result<Vec<Sam>>{
    // annotation for retuened value:(b,cigar,pos) where,
    //b: true when the read mapped to template strand,
    // cigar: cigar string for the alignment,
    // pos: First position of matched base
    Ok(BufReader::new(File::open(file)?).lines().filter_map(|e|e.ok())
       .skip_while(|e|e.starts_with('@'))
       .filter_map(|e|Sam::new(&e))
       .collect())
}


#[derive(Debug,Clone)]
pub struct Sam{
    q_name:String,
    flag:u32,
    r_name:String,
    pos:usize,
    mapq:usize,
    cigar:Vec<Op>,
    seq:String,
    qual:Vec<u8>,
}

impl Sam{
    fn new(input:&str)->Option<Self>{
        let contents:Vec<_> = input.split('\t').collect();
        Some(Sam{
            q_name:contents[0].to_string(),
            flag:contents[1].parse().ok()?,
            r_name:contents[2].to_string(),
            pos:contents[3].parse().ok()?,
            mapq:contents[4].parse().ok()?,
            cigar:parse_cigar_string(&contents[5]),
            seq:contents[9].to_string(),
            qual:contents[10].chars()
                .map(|e|e as u8 - 33)
                .collect(),
        })
    }
    pub fn is_primary(&self)->bool{
        (self.flag & 0x100) != 0x100
    }
    pub fn is_template(&self)->bool{
        (self.flag  & 0b10000) == 0b10000
    }
    fn to_coverage(&self)->Coverage{
        let mut cov = vec![];
        let mut start = self.pos;// reference position
        for op in &self.cigar{
            use self::Op::*;
            match *op{
                Align(b) | Match(b) => {
                    for i in 0..b{
                        cov.push((start+i,1));
                    }
                    start += b;
                }
                Insertion(_)|SoftClip(_)|HardClip(_)|Padding(_) => {},
                Deletion(b)|Skipped(b)|Mismatch(b) => {start +=b},
            }
        }
        Coverage{r_name:self.r_name.to_string(),
                 cov:cov}
    }
}

#[derive(Debug,Clone,Copy)]
enum Op{
    Align(usize),//M
    Insertion(usize),//I
    Deletion(usize),//D
    Skipped(usize),//N
    SoftClip(usize),//S
    HardClip(usize),//H
    Padding(usize),//P
    Match(usize),//=
    Mismatch(usize),//X
}

impl Op{
    fn new(op:&str)->Option<Op>{
        let (operation,num):(char,usize) = {
            let mut op = String::from(op);
            let operation = op.pop()?;
            (operation,op.parse().ok()?)
        };
        match operation{
            'M' => Some(Op::Align(num)),
            'I' => Some(Op::Insertion(num)),
            'D' => Some(Op::Deletion(num)),
            'N' => Some(Op::Skipped(num)),
            'S' => Some(Op::SoftClip(num)),
            'H' => Some(Op::HardClip(num)),
            'P' => Some(Op::Padding(num)),
            '=' => Some(Op::Match(num)),
            'X' => Some(Op::Mismatch(num)),
            _ => None
        }
    }
}



fn parse_cigar_string(cigar:&str)->Vec<Op>{
    lazy_static!{
        static ref RE:Regex = Regex::new(r"\d*[MIDNSHP=X]").unwrap();
    }
    RE.find_iter(cigar).filter_map(|e|Op::new(e.as_str())).collect()
}

#[test]
fn cigar_parse(){
    let cigar = "101S33M2I66M";
    let processed = parse_cigar_string(&cigar);
    eprintln!("{:?}",processed);
    panic!();
}

