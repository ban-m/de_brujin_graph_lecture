extern crate rand;
extern crate bio;
extern crate regex;
extern crate rayon;
#[macro_use] extern crate lazy_static;
pub mod utils;
