extern crate rand;
extern crate bio;
extern crate regex;
extern crate e_coli_coverage;
extern crate rayon;
use std::io;
use rand::{Rng,thread_rng};
use std::path::Path;
use e_coli_coverage::utils;

fn main()->io::Result<()>{
    let args:Vec<_> = std::env::args().collect();
    let mut reads = utils::load_sam_file(&Path::new(&args[1]))?;
    let pickup:usize = args[2].parse().unwrap();
    let mut rng = thread_rng();
    rng.shuffle(&mut reads);
    let coverages = utils::into_coverage(&reads[0..pickup]);
    println!("{:?}",reads[0]);
    for coverage in coverages{
        for (index,depth) in coverage.get_cov(){
            println!("{}\t{}\t{}",coverage.r_name(),index,depth)
        }
    }
    Ok(())
}
    


