extern crate rand;
extern crate bio;
use std::path::Path;
use bio::io::fastq;
use std::collections::HashMap;
fn main()-> std::io::Result<()>{
    let args:Vec<_> = std::env::args().collect();
    let k:usize = args[2].parse().unwrap();
    let reads:Vec<_> = fastq::Reader::from_file(&Path::new(&args[1]))?
    .records().filter_map(|e|e.ok()).collect();
    let mut result:HashMap<Vec<u8>
                           ,u64> = HashMap::new();
    for read in reads.into_iter(){
        let seq = read.seq();
        for kmer in seq.windows(k){
            result.entry(kmer.to_vec()).and_modify(|e|*e+=1)
                .or_insert(1);
        }
    }
    for (kmer,count) in result.iter(){
        let kmer = String::from_utf8(kmer.to_vec()).unwrap();
        println!("{}\t{}",kmer,count);
    }
    Ok(())
}
