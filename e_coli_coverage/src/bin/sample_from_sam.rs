extern crate rand;
extern crate bio;
extern crate regex;
extern crate e_coli_coverage;
extern crate rayon;
use std::io;
use rand::{Rng,thread_rng};
use std::path::Path;
use std::fs::File;
use std::io::{BufWriter,Write};
use e_coli_coverage::utils;
fn main()->io::Result<()>{
    let args:Vec<_> = std::env::args().collect();
    let reads = utils::load_sam_file(&Path::new(&args[1]))?;
    let mut reads:Vec<_> = reads.into_iter().filter(|e|e.is_primary()).collect();
    let from:usize = args[2].parse().unwrap();
    let to:usize = args[3].parse().unwrap();
    let by:usize = args[4].parse().unwrap();
    let mut rng = thread_rng();
    let sizes:Vec<_> = (0..).map(|e|from + e*by)
        .take_while(|&e|e<=to).collect();
    for sample_size in sizes{
        let mut wtr = BufWriter::new(File::create(
            format!("./result/sample{}.wig",sample_size))?);
        rng.shuffle(&mut reads);
        let coverages = utils::into_coverage(&reads[0..sample_size]);
        for coverage in coverages{
            for (index,depth) in coverage.get_cov(){
                writeln!(&mut wtr,"{}\t{}\t{}",coverage.r_name(),index,depth)?;
            }
        }
    }
    Ok(())
}
    


