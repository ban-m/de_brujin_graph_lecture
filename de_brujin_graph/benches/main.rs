#![feature(test)]
extern crate test;
extern crate bio;
extern crate de_brujin_graph;
use bio::io::fasta;
use de_brujin_graph::DeBrujinGraph;
use std::path::Path;
const FASTA:&str="./data/reads.fa";
const K:usize = 60;
#[cfg(test)]
use test::Bencher;

#[bench]
fn clone_and_eular(b:&mut Bencher){
    let fasta: Vec<_> = fasta::Reader::from_file(&Path::new(FASTA)).unwrap()
        .records()
        .filter_map(|e| e.ok())
        .collect();
    let k: usize = K;
    let mut de_brujin_graph = DeBrujinGraph::new(&fasta, k);
    b.iter(||{
        de_brujin_graph.clone().eular_all()
    })
}

#[bench]
fn clone(b:&mut Bencher){
    let fasta: Vec<_> = fasta::Reader::from_file(&Path::new(FASTA)).unwrap()
        .records()
        .filter_map(|e| e.ok())
        .collect();
    let k: usize = K;
    let mut de_brujin_graph = DeBrujinGraph::new(&fasta, k);
    b.iter(||{
        de_brujin_graph.clone()
    })
}
