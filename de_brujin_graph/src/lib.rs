extern crate bio;
extern crate rand;
use bio::io::fasta;
use std::collections::BTreeSet;
use std::collections::HashMap;

#[derive(Debug,Clone)]
pub struct DeBrujinGraph {
    k: usize,
    nodes: Vec<Kmer>,
    edges: Vec<BTreeSet<usize>>,
}

impl DeBrujinGraph {
    pub fn new(fasta: &[fasta::Record], k: usize) -> Self {
        // while construction, all kmer I've met would be recorded in
        // a hashmap named 'so_far', and it's bundled with the index of node
        // corresponds to the vector.
        let length: usize = fasta.iter().map(|e| e.seq().len()).sum();
        let mut nodes: Vec<_> = std::vec::Vec::with_capacity(length);
        let mut edges: Vec<BTreeSet<usize>> = std::vec::Vec::with_capacity(length);
        let mut so_far: HashMap<&[u8], usize> = HashMap::new();
        for read in fasta {
            let mut prev_index = None;
            for kmer in read.seq().windows(k) {
                let index = *so_far.entry(&kmer).or_insert_with(|| {
                    // current kmer is new element.
                    // Push it into nodes, and return its index.
                    nodes.push(Kmer::new(kmer));
                    edges.push(BTreeSet::new());
                    nodes.len() - 1
                });
                if let Some(prev) = prev_index {
                    // if previous index exists, it means that this is not the
                    // first element in the loop of read.
                    edges[prev as usize].insert(index);
                }
                prev_index = Some(index);
            }
        }
        DeBrujinGraph { k, nodes, edges }
    }
    pub fn nodes(&self) -> &[Kmer] {
        &self.nodes
    }
    pub fn eular_all(&mut self) -> Option<Vec<String>> {
        let mut result = vec![];
        while let Some(idx) = self.has_eular() {
            let path = self.eular_from(idx)?;
            result.push(path);
        }
        Some(result)
    }
    pub fn eular(&mut self) -> Option<String> {
        // Note that this function only give a Eular path of
        // one independent component.
        // If you want to enumerate all Eular paths/cycles,
        // use eular_all() instead.
        let start_idx = self.has_eular()?;
        self.eular_from(start_idx)
    }
    pub fn eular_from(&mut self, start_idx: usize) -> Option<String> {
        // Traversing Eularian path in the de Brujin Graph.
        // I emply a vector named stack to do depth first search.
        // Note that the stack is not to indicate the node
        // TO BE ARRIVED, rather, I intended to use it
        // as the exactly STACK in the processer or
        // recursive call.
        let mut stack: Vec<usize> = vec![];
        let mut path: Vec<usize> = vec![];
        // check if there are corresponding element to start kmer
        let mut idx = start_idx;
        loop {
            if let Some(child) = self.edges[idx].iter().nth(0).map(|e| *e) {
                // Proceed search while current node has at least one child.
                stack.push(idx);
                self.edges[idx].remove(&child);
                idx = child;
            } else {
                // Else, poping stack until reaching some node
                // which has a another child.
                path.push(idx);
                if let Some(poped) = stack.pop() {
                    idx = poped;
                } else {
                    break self.get_path(&path); //Exhaustive all stacks
                }
            }
        }
    }
    pub fn has_eular(&self) -> Option<usize> {
        //if self has eular cycle or path, return Some(index), where
        // nodes[index] is one of the starting point of Eular path/cycle.
        // relative_deg > 0 => there are more outdeg than indeg
        let mut relative_deg: Vec<Option<i32>> = vec![None; self.nodes.len()];
        for u in 0..self.nodes.len() {
            for &v in self.edges[u].iter() {
                // u -> v
                {
                    let u_deg = relative_deg[u].get_or_insert(0);
                    *u_deg += 1;
                }
                {
                    let v_deg = relative_deg[v].get_or_insert(0);
                    *v_deg -= 1;
                }
            }
        }
        if relative_deg.iter().all(|e|e.is_none()){
            None
        }else if relative_deg.iter().all(|e| e == &Some(0)){
            Some(0)
        }else{
            // There are exactly two index which has -1 or +1 relative deg.
            let mut plus = None;
            let mut minus = None;
            for (idx, deg) in relative_deg.iter().filter_map(|&e| e).enumerate() {
                if deg == 1 {
                    plus = match plus{
                        None => Some(idx),
                        Some(_) => return None,
                    }
                } else if deg == -1 {
                    minus =  match minus {
                        None => Some(idx),
                        Some(_) =>return None,
                    }
                } else if deg != 0 {
                    return None;
                }
            }
            if plus.is_some() && minus.is_some() {
                plus
            } else {
                None
            }
        }
    }
    fn get_path(&self, path: &[usize]) -> Option<String> {
        //println!("Retrieving...");
        let path: Vec<_> = path.into_iter().rev().collect();
        // for index in &path{
        //     print!("{}->",self.nodes[**index]);
        // }
        // println!("");
        let len = path.len();
        let mut result = self.nodes[*path[0]].text_as_u8().to_vec();
        for i in 1..len {
            result.push(self.nodes[*path[i]].last());
        }
        std::string::String::from_utf8(result).ok()
    }
}

impl std::fmt::Display for DeBrujinGraph {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        writeln!(f, "K:{}", self.k)?;
        writeln!(f, "Nodes:{}", self.nodes.len())?;
        writeln!(
            f,
            "Edges:{}",
            self.edges.iter().map(|e| e.len()).sum::<usize>()
        )?;
        for (index, edge) in self.edges.iter().enumerate() {
            let kmer = &self.nodes[index];
            writeln!(f, "From {}", kmer)?;
            for index in edge.iter() {
                let kmer = &self.nodes[*index];
                writeln!(f, "\tTo {}", kmer)?;
            }
        }
        Ok(())
    }
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Clone)]
pub struct Kmer {
    kmer: Vec<u8>,
}

impl std::fmt::Display for Kmer {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let kmer = std::string::String::from_utf8(self.kmer.clone()).unwrap_or("".to_string());
        write!(f, "{}", kmer)
    }
}

impl Kmer {
    pub fn new(kmer: &[u8]) -> Self {
        Kmer {
            kmer: kmer.to_vec(),
        }
    }
    fn text_as_u8(&self) -> &[u8] {
        &self.kmer
    }
    fn last(&self) -> u8 {
        self.kmer[self.kmer.len() - 1]
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
