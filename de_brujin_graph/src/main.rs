extern crate bio;
extern crate de_brujin_graph;
use bio::io::fasta;
use de_brujin_graph::DeBrujinGraph;
use std::path::Path;
fn main() -> std::io::Result<()> {
    let args: Vec<_> = std::env::args().collect();
    let fasta: Vec<_> = fasta::Reader::from_file(&Path::new(&args[1]))?
        .records()
        .filter_map(|e| e.ok())
        .collect();
    let k: usize = args[2].parse().expect("num");
    let mut de_brujin_graph = DeBrujinGraph::new(&fasta, k);
    println!("{}", de_brujin_graph);
    println!("{}", de_brujin_graph.eular().unwrap());
    if let Some(paths) = de_brujin_graph.eular_all(){
        for path in paths{
            println!("{}", path);
        }
    }
    //println!("{}", de_brujin_graph);
    Ok(())
}
